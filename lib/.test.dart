import 'package:flutter/material.dart';

void main() {
  testWidgets('MyApp displays "Hello, world!"', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Verify that the text is displayed.
    final textFinder = find.text('Hello, world!');
    expect(textFinder, findsOneWidget);
  });
}
